import React, {Component} from 'react';
import './myProfile.less';

class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "default name",
      gender: "Male",
      read: 'read',
      description: ''
    }
  }

  handlerChangeInput(even) {
    this.setState({name: even.currentTarget.value})
  }

  handlerChangeSelect(even) {
    this.setState({gender: even.currentTarget.value})
  }

  handlerSubmit() {
    const {name, gender, read, description} = this.state;
    console.log(name, gender, read, description);
  }

  handlerChangeCheckBox(even) {
    console.log(even.currentTarget.value)
  }

  handlerChangeDescription(even) {
    this.setState({description: even.currentTarget.value})
  }

  render() {
    const {name, gender, read, description} = this.state;
    return (
      <form onSubmit={this.handlerSubmit.bind(this)}>
        <h1>My Profile</h1>
        <label>
          Name
          <input placeholder='Your name' value={name} onChange={this.handlerChangeInput.bind(this)}/>
        </label>
        <label>
          Gender
          <select onChange={this.handlerChangeSelect.bind(this)} value={gender}>
            <option value='Male'>Male</option>
            <option value='Female'>Female</option>
          </select>
        </label>
        <label>
          Description
          <textarea
            placeholder='Description about yourself'
            value={description}
            onChange={this.handlerChangeDescription.bind(this)}/>
        </label>
        <label>
          <input type='checkbox' value={read} onChange={this.handlerChangeCheckBox.bind(this)}/> I have read
        </label>
        <button type='submit'>Submit</button>
      </form>
    );
  }
}

export default MyProfile;


